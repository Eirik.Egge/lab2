package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    List<FridgeItem> itemsInFridge = new ArrayList<>();
    final int maxCap = 20;


    @Override
    public int nItemsInFridge() {
        return itemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return maxCap;
    }

    @Override
    public void emptyFridge() {
        itemsInFridge.clear();

    }

    @Override
    public List<INF101.lab2.FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item : itemsInFridge) {
            if(item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        for (FridgeItem item : expiredItems) {
            if (item.hasExpired()) {
                itemsInFridge.remove(item);
            }
        }
        return expiredItems;
    }

    @Override
    public void takeOut(INF101.lab2.FridgeItem item) {
        if (nItemsInFridge() > 0) {
            itemsInFridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }

    }

    @Override
    public boolean placeIn(INF101.lab2.FridgeItem item) {
        if (nItemsInFridge() < maxCap) {
            itemsInFridge.add(item);
            return true;
        }
        return false;
    }
}
